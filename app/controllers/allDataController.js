import {query, getFichesBySiteQuery, getStatutsQuery, getUsersBySiteQuery, getNameSiteByIdQuery, getDispositifBySiteQuery,
        getParametresQuery, getTypesInterventionQuery, getModOpBySiteQuery, getUniteQuery, getMethodesBySiteQuery, getLinkFicheInterventionByFicheQuery,
        getInterventionByIdQuery, getMesuresByInterventionsQuery, upadteFicheStatutQuery, getParcellesBySiteQuery, getEquipementsBySiteQuery,
        getLotsAnimauxQuery, getRessourcesQuery, getEchantillonsQuery, getElementsPaysageQuery, getStatutsSaisiesQuery, getAnimauxBySiteQuery,
        insertSaisieQuery, insertInterventionQuery, insertInterventionAnimalQuery, insertInterventionLotQuery, insertInterventionEquipementQuery, insertInterventionElementQuery,
        insertInterventionRessourceQuery, insertInterventionParcelleQuery, insertInterventionEchantillonQuery, insertInterventionUtilisateurQuery,
        insertMesureQuery, insertMesureEquipementQuery, insertMesureValeurQuery, insertParametreValeurQuery} from '../db/dbQuery'
import {errorMessage, successMessage, status,} from '../helpers/status'

/**
 * Get all information about sheets and dependency
 * @param {*} req 
 * @param {*} res 
 * 
 * @return {object} response
 */
const getFichesAndDependency = async (req, res) => {
    const idSite = req.params.site ? req.params.site : "-1"
    try {
        successMessage.data = {}

        var resultQuery = await query(getNameSiteByIdQuery, [idSite])
        const nameSite = resultQuery.rows[0].sit_nom

        // Add parametres table
        resultQuery = await query(getParametresQuery)
        successMessage.data.parametres = resultQuery.rows

        // Add unite table
        resultQuery = await query(getUniteQuery)
        successMessage.data.unites = resultQuery.rows

        // Add statuts fiche table
        resultQuery = await query(getStatutsQuery)
        successMessage.data.statuts = resultQuery.rows

        // Add statuts saisies table
        resultQuery = await query(getStatutsSaisiesQuery)
        successMessage.data.statutsaisie = resultQuery.rows

        // Add typesintervention table
        resultQuery = await query(getTypesInterventionQuery)
        successMessage.data.typesintervention = resultQuery.rows

        // Add lot animaux table 
        resultQuery = await query(getLotsAnimauxQuery)
        successMessage.data.lotsanimaux = resultQuery.rows

        // Add ressources table
        resultQuery = await query(getRessourcesQuery)
        successMessage.data.ressources = resultQuery.rows

        // Add echantillons table
        resultQuery = await query(getEchantillonsQuery)
        successMessage.data.echantillons = resultQuery.rows

        // Add element table 
        resultQuery = await query(getElementsPaysageQuery)
        successMessage.data.elements = resultQuery.rows

        // Add animal table
        resultQuery = await query(getAnimauxBySiteQuery,[nameSite])
        successMessage.data.animaux = resultQuery.rows
        
        // Add users table
        resultQuery = await query(getUsersBySiteQuery, [nameSite]);
        successMessage.data.users = resultQuery.rows

        // Add dispositif table
        resultQuery = await query(getDispositifBySiteQuery, [nameSite]);
        successMessage.data.dispositifs = resultQuery.rows

        // Add ModeOp table 
        resultQuery = await query(getModOpBySiteQuery, [nameSite]);
        successMessage.data.modop = resultQuery.rows

        // Add methode table 
        resultQuery = await query(getMethodesBySiteQuery, [nameSite]);
        successMessage.data.methodes = resultQuery.rows

        // Add parcelles table
        resultQuery = await query(getParcellesBySiteQuery, [nameSite]);
        successMessage.data.parcelles = resultQuery.rows

        // Add equipements table
        resultQuery = await query(getEquipementsBySiteQuery, [nameSite]);
        successMessage.data.equipements = resultQuery.rows


        // Add fiches table
        resultQuery = await query(getFichesBySiteQuery, [idSite])
        const rowsFiches = resultQuery.rows
        successMessage.data.fiches = rowsFiches

        // Add link betwenn fiche and intervention table
        successMessage.data.linkficheintervention = []
        successMessage.data.interventions = []
        successMessage.data.mesures = []
        for (var i = 0; i < rowsFiches.length; i++){
            resultQuery = await query(getLinkFicheInterventionByFicheQuery, [rowsFiches[i].fic_id])
            const rowsLinkFicheInt = resultQuery.rows
            successMessage.data.linkficheintervention = successMessage.data.linkficheintervention.concat(rowsLinkFicheInt)

            // Add interventions tables
            for(var j=0; j < rowsLinkFicheInt.length ; j ++){
                resultQuery = await query(getInterventionByIdQuery, [rowsLinkFicheInt[j].fli_intervention_id])
                const rowsInterventions = resultQuery.rows
                successMessage.data.interventions = successMessage.data.interventions.concat(rowsInterventions)

                // Add mesure table
                for(var k = 0; k < rowsInterventions.length; k ++){
                    resultQuery = await query(getMesuresByInterventionsQuery, [rowsInterventions[k].lii_id])
                    const rowsMesures = resultQuery.rows
                    successMessage.data.mesures = successMessage.data.mesures.concat(rowsMesures)
                }
            }
            await query(upadteFicheStatutQuery, [rowsFiches[i].fic_id, 3])
        }

        

        return res.status(status.success).send(successMessage)

    } catch (error) {
        console.log(error)
        errorMessage.error = 'Operation was not successful'
        return res.status(status.error).send(errorMessage)
    }
}

/**
 * Post close entry
 * @param {*} req 
 * @param {*} res 
 */
const postSaisies = async (req, res) => {

    try {
        console.log(req.body)
        const saisies = req.body.saisies
        // Add saisie
        for(var i in saisies) {
            const currentSaisie = saisies[i]
            var resultQuery = await query(insertSaisieQuery, [currentSaisie.sas_creation, currentSaisie.sas_modification, currentSaisie.sas_utilisateur, 2, currentSaisie.sas_fiche])
            const idSaisie = resultQuery.rows[0].sas_id

            // Add intervention
            for (var j in currentSaisie.interventions){
                const currentIntervention = currentSaisie.interventions[j]
                resultQuery = await query(insertInterventionQuery, [idSaisie, currentIntervention.int_type, currentIntervention.int_modeop])
                const idIntervention = resultQuery.rows[0].int_id

                // Add animal
                for(var k in currentIntervention.animaux){
                    resultQuery = await query(insertInterventionAnimalQuery, [idIntervention, currentIntervention.animaux[k]])
                }
                
                // Add lots
                for(var k in currentIntervention.lots){
                    resultQuery = await query(insertInterventionLotQuery, [idIntervention, currentIntervention.lots[k]])
                }

                // Add equipements
                for(var k in currentIntervention.equipements){
                    resultQuery = await query(insertInterventionEquipementQuery, [idIntervention, currentIntervention.equipements[k]])
                }

                // Add elements
                for(var k in currentIntervention.elements){
                    resultQuery = await query(insertInterventionElementQuery, [idIntervention, currentIntervention.elements[k]])
                }

                // Add ressources
                for(var k in currentIntervention.ressources){
                    resultQuery = await query(insertInterventionRessourceQuery, [idIntervention, currentIntervention.ressources[k]])
                }

                // Add parcelles
                for(var k in currentIntervention.parcelles){
                    resultQuery = await query(insertInterventionParcelleQuery, [idIntervention, currentIntervention.parcelles[k]])
                }

                // Add echantillons
                for(var k in currentIntervention.echantillons){
                    resultQuery = await query(insertInterventionEchantillonQuery, [idIntervention, currentIntervention.echantillons[k]])
                }

                // Add utilisateurs
                for(var k in currentIntervention.utilisateurs){
                    resultQuery = await query(insertInterventionUtilisateurQuery, [idIntervention, currentIntervention.utilisateurs[k]])
                }

                // Add mesures
                for(var k in currentIntervention.mesures){
                    const currentMesure = currentIntervention.mesures[k]
                    resultQuery = await query(insertMesureQuery, [currentMesure.mes_parcelle, currentMesure.mes_methode, currentMesure.mes_echantillon, idIntervention])
                    const idMesure = resultQuery.rows[0].mes_id

                    // Add equipement de mesure
                    for (var l in currentMesure.equipements){
                        resultQuery = await query(insertMesureEquipementQuery, [idMesure, currentMesure.equipements[l]])
                    }

                    // Add valeur
                    resultQuery = await query(insertMesureValeurQuery, [idMesure, currentMesure.valeur])
                }

                // add parametres
                for(var k in currentIntervention.parametres){
                    const currParametre = currentIntervention.parametres[k]
                    resultQuery = await query(insertParametreValeurQuery,[idIntervention, currParametre.vpa_parametre, currParametre.vpa_value] )
                }
            }

            // Statut  fiche = cloture
            await query(upadteFicheStatutQuery, [currentSaisie.sas_fiche, 4])

        }
        return res.status(status.success).send(successMessage)

    } catch(error){
        console.log(error)
        errorMessage.error = 'Operation was not successful'
        return res.status(status.error).send(errorMessage)
    }
}

export {getFichesAndDependency, postSaisies}