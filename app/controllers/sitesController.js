
import {query, getSitesQuery} from '../db/dbQuery'
import {errorMessage, successMessage, status,} from '../helpers/status'

/**
 * Get all sites
 * @param {*} req 
 * @param {*} res 
 * 
 * @return {object} response
 */
const getSites = async (req, res) => {

  try {
    const { rows } = await query(getSitesQuery);
    successMessage.data = rows
    return res.status(status.success).send(successMessage)
  } catch (error) {
    errorMessage.error = 'Operation was not successful'
    return res.status(status.error).send(errorMessage)
  }
}

export {getSites}