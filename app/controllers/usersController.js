
import {query, signinUserQuery, getUsersBySiteQuery} from '../db/dbQuery'
import {
  comparePassword,
  isValidEmail,
  validatePassword,
  isEmpty,
} from '../helpers/validations'
import { generateUserToken, readBasicAuth} from '../helpers/authorization'
import {errorMessage, successMessage, status,} from '../helpers/status'

/**
   * Signin
   * @param {object} req
   * @param {object} res
   * @returns {object} user object
   */
const siginUser = async (req, res) => {
  var auth64 = req.headers['authorization']

  if(auth64){
    var auth = readBasicAuth(auth64)
    const email = auth.username
    const password = auth.password

    if (isEmpty(email) || isEmpty(password)) {
      errorMessage.error = 'Email or Password detail is missing'
      return res.status(status.bad).send(errorMessage)
    }
    if (!isValidEmail(email) || !validatePassword(password)) {
      errorMessage.error = 'Please enter a valid Email or Password'
      return res.status(status.bad).send(errorMessage)
    }

    try {
      const { rows } = await query(signinUserQuery, [email]);
      const dbResponse =  rows.length > 0 ? rows[0] : undefined
      if (!dbResponse || !comparePassword(dbResponse.uti_mobile_mdp, password)) {
        errorMessage.error = 'User with this email or this password does not exist'
        return res.status(status.notfound).send(errorMessage)
      }
      const token = generateUserToken(dbResponse.uti_login, dbResponse.uti_id, dbResponse.uti_prenom, dbResponse.uti_nom)
      successMessage.data = {token: token }
      return res.status(status.success).send(successMessage)
    } catch (error) {
      errorMessage.error = 'Operation was not successful'
      return res.status(status.error).send(errorMessage)
    }
  }
  else {
    errorMessage.error = 'Email or Password detail is missing'
    return res.status(status.bad).send(errorMessage)
  }
  
}

/**
 * Get Users by site id
 * 
 * @param {*} req 
 * @param {*} res 
 * 
 * @returns {object} response
 */
const getUsersBySite = async (req, res) => {
  const idSite = req.params.site ? req.params.site : "-1"
  try {
    const { rows } = await query(getUsersBySiteQuery, [idSite]);
    successMessage.data = rows
    return res.status(status.success).send(successMessage)
  } catch (error) {
    errorMessage.error = 'Operation was not successful'
    return res.status(status.error).send(errorMessage)
  }
}

export {siginUser, getUsersBySite}