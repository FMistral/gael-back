import pool from './pool';


/**
 * DB Query
 * @param {object} req
 * @param {object} res
 * @returns {object} object
 */
const query = (quertText, params) => {
  return new Promise((resolve, reject) => {
    pool.query(quertText, params)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

const signinUserQuery = 'SELECT * FROM public.ts_utilisateur_uti WHERE uti_login = $1'
const getUsersBySiteQuery = "SELECT uti_id, uti_nom, uti_prenom, dn, uti_login, uti_mobile_mdp FROM public.ts_utilisateur_uti INNER JOIN public.tj_utilisateursite_usi ON public.ts_utilisateur_uti.dn = public.tj_utilisateursite_usi.usi_uti_id WHERE public.tj_utilisateursite_usi.usi_sit_id=$1 "

const getSitesQuery = 'SELECT * FROM public.ts_site_sit'

const getFichesBySiteQuery = 'SELECT * FROM public.t_fiche_fic WHERE public.t_fiche_fic.fic_sit_id = $1 AND (fic_stf_id =2 OR fic_stf_id =3)'
const upadteFicheStatutQuery = 'UPDATE t_fiche_fic SET fic_stf_id = $2 WHERE fic_id = $1'
const getStatutsQuery = 'SELECT * FROM public.t_statut_st'
const getNameSiteByIdQuery = 'SELECT sit_nom FROM public.ts_site_sit where ts_site_sit.sit_id = $1'
const getDispositifBySiteQuery = 'SELECT * FROM public.ts_dispositif_dis WHERE ts_dispositif_dis.dis_site_id = $1'
const getParametresQuery = 'SELECT * FROM public.tr_parametre_paa'
const getTypesInterventionQuery = 'SELECT * FROM public.tr_typeintervention_tyi'
const getModOpBySiteQuery = 'SELECT * FROM public.tr_modeoperatoire_mod WHERE mod_sit_id = $1'
const getUniteQuery = 'SELECT * FROM public.tr_unite_uni'
const getMethodesBySiteQuery = 'SELECT * FROM public.tr_methode_met WHERE met_site_id = $1'
const getLinkFicheInterventionByFicheQuery = 'SELECT * FROM public.tj_ficheligneintervention_fli WHERE fli_fiche_id = $1'
const getInterventionByIdQuery = 'SELECT * FROM public.t_ligneintervention_lii WHERE lii_id = $1'
const getMesuresByInterventionsQuery = 'SELECT * FROM public.t_lignesmesure_lgm WHERE lgm_lii_id = $1'
const getParcellesBySiteQuery = 'SELECT * FROM public.t_parcelle_prc WHERE prc_site= $1'
const getEquipementsBySiteQuery = ' SELECT * FROM public.t_equipement_eqp WHERE eqp_site = $1'
const getLotsAnimauxQuery = 'SELECT * FROM public.t_lotanimaux_lax'
const getAnimauxBySiteQuery = 'SELECT * FROM t_animal_anm WHERE anm_site = $1'
const getRessourcesQuery = 'SELECT * FROM public.t_ressources_rsc'
const getEchantillonsQuery = 'SELECT * FROM public.t_echantillon_ech'
const getElementsPaysageQuery = 'SELECT * FROM public.t_element_elt'
const getStatutsSaisiesQuery ='SELECT * FROM t_statutsaisie_sts'

const insertSaisieQuery = "INSERT INTO t_saisie_sas (sas_creation, sas_modification, sas_utilisateur, sas_statut, sas_fiche) VALUES ( $1, $2, $3, $4, $5) RETURNING sas_id"
const insertInterventionQuery = "INSERT INTO t_intervention_int (int_saisie, int_type, int_modeop) VALUES ( $1, $2, $3) RETURNING int_id"
const insertInterventionAnimalQuery = "INSERT INTO t_interventionanimaux_ian (ian_intervention, ian_animal) VALUES ($1, $2)"
const insertInterventionLotQuery = "INSERT INTO t_interventionlotanimaux_ila (ila_intervention, ila_lotanimaux) VALUES ($1, $2)"
const insertInterventionEquipementQuery =  "INSERT INTO t_interventionequipement_ieq (ieq_intervention, ieq_equipement) VALUES ($1, $2)"
const insertInterventionElementQuery =  "INSERT INTO t_interventionelement_iel (iel_intervention, iel_element) VALUES ($1, $2)"
const insertInterventionRessourceQuery = "INSERT INTO t_interventionressource_irc (irc_intervention, irc_ressource) VALUES ($1, $2)"
const insertInterventionParcelleQuery = "INSERT INTO t_interventionparcelle_ipa (ipa_intervention, ipa_parcelle) VALUES ($1, $2)"
const insertInterventionEchantillonQuery = "INSERT INTO t_interventionechantillon_iec (iec_intervention, iec_echantillon) VALUES ($1, $2)"
const insertInterventionUtilisateurQuery = "INSERT INTO t_interventionutilisateurs_iut (iut_intervention, iut_utilisateur) VALUES ($1, $2)"
const insertMesureQuery = "INSERT INTO t_mesure_mes (mes_parcelle, mes_methode, mes_echantillon, mes_intervention) VALUES ($1, $2, $3, $4) RETURNING mes_id"
const insertMesureEquipementQuery = "INSERT INTO t_mesureequipement_meq (meq_mesure, meq_equipement) VALUES ($1, $2)"
const insertMesureValeurQuery = "INSERT INTO t_mesurevariable_mvr (mvr_mesure, mvr_value) VALUES ($1, $2)"
const insertParametreValeurQuery = "INSERT INTO t_valeurparametres_vpa (vpa_intervention, vpa_parametre, vpa_value) VALUES ($1, $2, $3)"

export {query, signinUserQuery, getSitesQuery, getUsersBySiteQuery,getFichesBySiteQuery, getStatutsQuery, getNameSiteByIdQuery,getDispositifBySiteQuery,
  getParametresQuery, getTypesInterventionQuery, getModOpBySiteQuery, getUniteQuery, getMethodesBySiteQuery, getLinkFicheInterventionByFicheQuery, 
  getInterventionByIdQuery, getMesuresByInterventionsQuery, upadteFicheStatutQuery, getParcellesBySiteQuery, getEquipementsBySiteQuery, 
  getLotsAnimauxQuery, getRessourcesQuery, getEchantillonsQuery,getElementsPaysageQuery, getStatutsSaisiesQuery, getAnimauxBySiteQuery,
  insertSaisieQuery, insertInterventionQuery, insertInterventionAnimalQuery, insertInterventionLotQuery, insertInterventionEquipementQuery, insertInterventionElementQuery,
  insertInterventionRessourceQuery, insertInterventionParcelleQuery, insertInterventionEchantillonQuery, insertInterventionUtilisateurQuery,
  insertMesureQuery, insertMesureEquipementQuery, insertMesureValeurQuery, insertParametreValeurQuery}