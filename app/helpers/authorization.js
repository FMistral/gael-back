import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import env from '../../env';

dotenv.config()

/**
 * Transform a Basic Auth in object {username, password}
 * 
 * @param string auth with Basic auth
 * 
 * @returns {object}
 */
const readBasicAuth = (auth) => {
    var tmp = auth.split(' ')
    var buf = Buffer.from(tmp[1], 'base64')
    var plain_auth = buf.toString()

    var creds = plain_auth.split(':') 
    return {username : creds[0], password: creds[1]}
}


/**
 * Generate token
 * 
 * @param string email 
 * @param string id 
 * @param string first_name 
 * @param string last_name 
 * 
 * @returns {string} token
 */
const generateUserToken = (email, id, first_name, last_name) => {
    const token = jwt.sign({
        email,
        user_id: id,
        first_name,
        last_name,
    },
    env.secret,{algorithm: "HS256", expiresIn: '1h'})
    return token;
};

export {generateUserToken, readBasicAuth};