import env from '../../env'
import jwt from 'jsonwebtoken'

/**
 * comparePassword
 * @param {string} dbPassword
 * @param {string} password
 * @returns {Boolean} return True or False
 */
const comparePassword = (dbPassword, password) => {
    return dbPassword === password
}

/**
 * isValidEmail helper method
 * @param {string} email
 * @returns {Boolean} True or False
 */
const isValidEmail = (email) => {
  const regEx = /\S+@\S+\.\S+/
  return regEx.test(email)
}

/**
 * validatePassword helper method
 * @param {string} password
 * @returns {Boolean} True or False
 */
const validatePassword = (password) => {
    var isValidate = false
    if (password.length === 4) {
        isValidate = true
    } 
    return isValidate
}


/**
 * isEmpty helper method
 * @param {string, integer} input
 * @returns {Boolean} True or False
 */
const isEmpty = (input) => {
    var isEmpty = false
    if (input === undefined || input === '') {
        isEmpty = true
    }
    if (!isEmpty && input.replace(/\s/g, '').length) {
        isEmpty = false
    } else {
        isEmpty = true
    }
    return isEmpty
}

export {
  isValidEmail,
  validatePassword,
  isEmpty, 
  comparePassword
}