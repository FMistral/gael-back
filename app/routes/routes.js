import express from 'express'
import { siginUser, getUsersBySite } from '../controllers/usersController'
import { verifyToken } from '../middlewares/authorization'
import { getSites } from '../controllers/sitesController'
import { getFichesAndDependency, postSaisies } from '../controllers/allDataController'

const router = express.Router()

router.get('/auth/signin', siginUser)

router.use(verifyToken)
router.get('/sites', getSites)
router.get('/users/sites/:site', getUsersBySite)
router.get('/fiches/sites/:site', getFichesAndDependency)
router.post('/saisies', postSaisies)

export default router